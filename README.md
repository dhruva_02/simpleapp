# SimpleApp



## Getting started
Follow the steps below to setup a pipeline and execute the program in Jenkins and setup GitLab pipeline to trigger Jenkins pipeline on commit:

### Step 1: Creating a pipeline in Jenkins
1. Install Jenkins in the local system or on cloud.
2. Create a new Pipeline item
3. Select `Pipeline script from SCM` and select `Git` under SCM.
4. Enter the URL of the repository and select the branch `main` for building.
5. Save the configuration and build.

### Step 2: Trigger Jenkins from GitLab
1. In Jenkins under the configuration select `Trigger builds remotely (e.g., from scripts)` and enter any string as an Authentication Token.
2. In GitLab modify the `.gitlab-ci.yml` file such that the location of the project in Jenkins in used as the `project` to trigger and enter the Authentication Token used in Jenkins as trigger `token` and save.

### Step 3: Testing and Validation
1. Run `Build Now` in Jenkins to check successful configuration of the Jenkins Pipeline.
2. Commit a change in GitLab to check if the Jenkins pipeline is triggered successfully.
