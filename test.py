import unittest
from random_parity import generate_random_number, check_even_odd

class TestRandomNumber(unittest.TestCase):
    def test_generate_random_number(self):
        # Test if the generated number is within the specified range
        random_number = generate_random_number()
        self.assertTrue(1 <= random_number <= 100)

    def test_check_even_odd(self):
        # Test if the function correctly identifies even and odd numbers
        self.assertEqual(check_even_odd(2), "Even")
        self.assertEqual(check_even_odd(3), "Odd")

if __name__ == '__main__':
    unittest.main()
